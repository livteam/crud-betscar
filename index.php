<!DOCTYPE html>
<html ng-modules="routerApp, Acesso, ModalWindow">
<head>
	<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<meta name="author" content="iLeonardo Carvalho" />
		<title-placeholder id="myTitle" ui-view="title"></title-placeholder>

		<!-- CSS | Estrutura -->
		<link rel="stylesheet"type="text/css" href="./css/icss.css">
		<link rel="stylesheet"type="text/css" href="./css/navleft.css">
		<link rel="stylesheet"type="text/css" href="./css/grid.css">
		<link rel="stylesheet"type="text/css" href="./css/modal.css">

		<!-- SCRIPT | SETUP -->
		<script type="text/javascript" src="./js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="./js/angular-1.4.6.js"></script>

		<!-- SCRIPT | CONFIG - ANGULARJS -->
		<script type="text/javascript" src="./js/ng-module-11.sep.2014.js"></script>
		<script type="text/javascript" src="./js/ui-router-0.2.15.js"></script>
		<script type="text/javascript" src="./js/state-files-22.jul.2014.js"></script>

		<!-- SCRIPT | Template -->
		<script type="text/javascript" src="./js/actionangular/route-page.js"></script>
		<script type="text/javascript" src="./js/actionangular/modal-window.js"></script>
		<script type="text/javascript" src="./js/actionangular/login.js"></script>

</head>
<body scroll="no" class="{{containerClass}}">

<div ui-view="nav"></div>
<div ui-view="nav-aside"></div>
<div ui-view="site"></div>

</body>
