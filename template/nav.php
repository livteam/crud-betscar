<nav class="row">
	<ul class="nav col">
	  <li><a ui-sref="home">Home</a></li>
	  <li><a ui-sref="#">About-us</a></li>
	  <li><a ui-sref="service">Serviços</a></li>
	  <li class="drop">Começando
		<ul>
		  <li><a ui-sref="#">Requisitos</a></li>
		  <li><a ui-sref="#">Semântica do Projeto</a></li>
		  <li><a ui-sref="#">Cores do Sistema</a></li>
		</ul>
	  </li>
	  <li class="drop">Customização
		<ul>
		  <li><a ui-sref="#">TAGS</a></li>
		  <li><a ui-sref="#">GRID</a></li>
		  <li><a ui-sref="#">BOX</a></li>
		  <li><a ui-sref="#">CARD</a></li>
		  <li><a ui-sref="#">LIST</a></li>
		  <li><a ui-sref="#">Inplementação Responsive</a></li>
		</ul>
	  </li>
	  <li class="drop">CSS3
		<ul>
		  <li><a ui-sref="#">NavBar</a></li>
		  <li><a ui-sref="#">Form</a></li>
		  <li><a ui-sref="#">Input</a></li>
		  <li><a ui-sref="#">Button</a></li>
		  <li><a ui-sref="#">a - Link</a></li>
		  <li><a ui-sref="#">Table</a></li>
		  <li><a ui-sref="#">IMG</a></li>
		  <li><a ui-sref="#">Text h1 - h6 | p</a></li>
		</ul>
	  </li>
	  <li>
	  		<input type="checkbox" id="responsive-nav" name="responsive-nav" />
			<label for="responsive-nav">☰</label>
	  </li>
	  <li class="drop float-right">Sistema
		<ul class="nav-right">
		  <li><a ui-sref="acesso">Acessar o Sistema</a></li>
		  <li><a ui-sref="#">Versão Compilada</a></li>
		  <li><a ui-sref="#">Contato</a></li>
		</ul>
	  </li>
	</ul>
</nav>