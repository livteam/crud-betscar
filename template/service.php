<header class="float-icon" id="header">
  <div class="col-duoicon">
		<h1 class="maiusculo">Serviços</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut quasi laborum, magnam. Temporibus 
		obcaecati asperiores impedit aliquid laudantium reiciendis veritatis!</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam explicabo magnam architecto 
		impedit ex eius eos consectetur natus ab doloremque error nostrum minus quia illum repudiandae, 
		necessitatibus nesciunt. Incidunt, accusamus.</p>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores atque labore, similique 
		neque soluta est corporis, ducimus repudiandae cupiditate nulla?</p>
		<button class="btn-default">Learm More</button>
  </div>
  <div class="col-duoicon">
		<img class="img table-cell" src="./img/camaro-car.jpg" alt="Camaro Car" />
  </div>
</header>

<section class="float-icon">
	<article class="col-icon">
		<section class="col">
			<article class="row">
				<img class="img table-cell container-fluid" src="./img/Au-KBC.jpg">
				<span class="wrapper">Quimica</span>
			</article>
			<article class="row">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, commodi?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quibusdam voluptate 
				obcaecati dolorem qui nobis, aut maxime. Ipsa non cum esse, eius a laborum, nam officiis 
				cupiditate expedita maiores iusto nostrum, quaerat quam accusamus, facilis ut. A quod 
				eum similique, laudantium molestiae dolorum saepe, nemo voluptatum distinctio tempore 
				ratione incidunt.</p>
				<button>Learm More</button>
			</article>
		</section>
	</article>
	<article class="col-icon">
		<section class="col">
			<article class="row">
				<img class="img table-cell container-fluid" src="./img/Best-Friends.jpeg">
				<span class="wrapper">Maçã</span>
			</article>
			<article class="row">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, commodi?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quibusdam voluptate 
				obcaecati dolorem qui nobis, aut maxime. Ipsa non cum esse, eius a laborum, nam officiis 
				cupiditate expedita maiores iusto nostrum, quaerat quam accusamus, facilis ut. A quod 
				eum similique, laudantium molestiae dolorum saepe, nemo voluptatum distinctio tempore 
				ratione incidunt.</p>
				<button>Learm More</button>
			</article>
		</section>
	</article>
	<article class="col-icon">
		<section class="col">
			<article class="row">
				<img class="img table-cell container-fluid" src="./img/Week-In-Our-Shoes.jpg">
				<span class="wrapper">Pessoas</span>
			</article>
			<article class="row">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, commodi?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quibusdam voluptate 
				obcaecati dolorem qui nobis, aut maxime. Ipsa non cum esse, eius a laborum, nam officiis 
				cupiditate expedita maiores iusto nostrum, quaerat quam accusamus, facilis ut. A quod 
				eum similique, laudantium molestiae dolorum saepe, nemo voluptatum distinctio tempore 
				ratione incidunt.</p>
				<button>Learm More</button>
			</article>
		</section>
	</article>
</section>

<footer id="footer">
<p>Responsive iCSS V1.0 - Código Aberto | Copyright @ 2015. <a class="link" ui-sref="home">iRedes Computer</a>. Todos os direitos reservados. | Licença <code>Apache 2.0</code> - <a class="link" ui-sref="home">GitHub</a></p>
</footer>
