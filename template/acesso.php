		<!-- SCRIPT | JQUERY -->
		<script type="text/javascript" src="./js/actionjquery/estados-cidades.js"></script>

<div ng-controller="ModalView">

<div class="md-8">
	<section class="box">
	<header class="title fundo-green1"></header>
	<header class="subtitle fundo-green2">Login</header>
		<article class="fundo-card" ng-controller="FormLogin">
		  <form class="float-label" spellcheck="false" ng-submit="credenciais()">
		  	<legend>Logon User</legend>

			  <div class="control">
				<input type="text" name="usuario" ng-model="usuario" placeholder="Usuário" required />
				<label for="usuario">Usuário</label>
			  </div>

			  <div class="control medium">
				<input type="number" name="cpf" ng-model="cpf" placeholder="CPF" required pattern="[0-9]+$" />
				<label for="cpf">CPF</label>
			  </div>

			  <div class="control medium">
				<input type="password" name="senha" ng-model="senha" placeholder="Senha" required />
				<label for="location">Senha</label>
			  </div>

			  <div class="control medium">
				<input type="button" ng-click="open()"class="btn-default" value="Fazer Registro" />
			  </div>
			  <div class="control medium">
				<input type="submit" name="login" id="login" value="Fazer Login" />
			  </div>
		  </form>
		</article>
		<footer class="footer-card wrapper fundo-green2">
			<span>Copyright</span> <span>-| BetsCar V1.0 |-</span> <span>© 2015 | iCSS</span>
		</footer>
	</section>
</div>

<div modal="showModal" close="cancel()">
	<header class="modal-header">
	  <h1 class="modal-header-title">Registrar</h1>
	</header>
	<section class="modal-body">
		<article class="modal-content" ng-controller="FormRegistro">
		  <form class="float-label" spellcheck="false" ng-submit="credenciais_reg()">
			<legend>Register User</legend>

			  <div class="control">
				<input type="text" name="usuario" ng-model="usuario" placeholder="Nome do Usuário" required />
				<label for="usuario">Nome do Usuário</label>
			  </div>

			  <div class="control medium">
				<input type="number" name="cpf" ng-model="cpf" placeholder="CPF" pattern="[0-9]+$" required />
				<label for="cpf">CPF</label>
			  </div>

			  <div class="control medium">
				<input type="password" name="senha" ng-model="senha" placeholder="Senha" required />
				<label for="location">Senha</label>
			  </div>

			  <div class="control med">
				<input type="number" name="cnh" ng-model="cnh" placeholder="CNH" pattern="[0-9]+$" required />
				<label for="cnh">CNH</label>
			  </div>

			  <div class="control small">
				<input type="number" name="nvacesso" ng-model="nvacesso" placeholder="1" required disabled />
				<label for="nvacesso">Nível de Acesso | Default</label>
			  </div>

			  <div class="control small">
				<input type="number" name="rg" ng-model="rg" placeholder="RG" pattern="[0-9]+$" required />
				<label for="rg">RG</label>
			  </div>

			  <div class="control small">
				<input type="number" name="contato" ng-model="contato" placeholder="Contato" />
				<label for="contato">Contato</label>
			  </div>

			  <div class="control medium">
				<input type="date" name="dtdata" ng-model="dtdata" placeholder="Data de Nascimento" />
				<label for="dtdata">Data de Nascimento</label>
			  </div>

			  <div class="control small">
				<input type="text" name="bairro" ng-model="bairro" placeholder="Bairro e/ou Setor" />
				<label for="bairro">Bairro e/ou Setor</label>
			  </div>

			  <div class="control small">
				<select id="estado" name="estado">
					<option value="" disabled selected># - Estado</option>
					<option value=""></option>
					<option value="AC">Acre</option>
					<option value="AL">Alagoas</option>
					<option value="AM">Amazonas</option>
					<option value="AP">Amapá</option>
					<option value="BA">Bahia</option>
					<option value="CE">Ceará</option>
					<option value="DF">Distrito Federal</option>
					<option value="ES">Espírito Santo</option>
					<option value="GO">Goiás</option>
					<option value="MA">Maranhão</option>
					<option value="MT">Mato Grosso</option>
					<option value="MS">Mato Grosso do Sul</option>
					<option value="MG">Minas Gerais</option>
					<option value="PA">Pará</option>
					<option value="PB">Paraíba</option>
					<option value="PR">Paraná</option>
					<option value="PE">Pernambuco</option>
					<option value="PI">Piauí</option>
					<option value="RJ">Rio de Janeiro</option>
					<option value="RN">Rio Grande do Norte</option>
					<option value="RO">Rondônia</option>
					<option value="RS">Rio Grande do Sul</option>
					<option value="RR">Roraima</option>
					<option value="SC">Santa Catarina</option>
					<option value="SE">Sergipe</option>
					<option value="SP">São Paulo</option>
					<option value="TO">Tocantins</option>
				</select>
				<label for="estado">Estado</label>
			  </div>

			  <div class="control medium">
				<select id="cidade" name="cidade" ng-model="cidade">
					<option value="" disabled selected>Cidade</option>
				</select>
				<label for="cidade">Cidade</label>
			  </div>

			  <div class="control medium">
				<input type="submit" name="login" id="login" value="Fazer Registro" ng-click="ok()" />
			  </div>

			  <div class="control medium">
				<input type="button" class="btn-default" value="Cancelar" ng-click="cancel()" />
			  </div>

		  </form>
		</article>
	</section>
	<footer class="modal-footer wrapper">
	  <code>O registro é obrigatório dados reais do Usuário.</code>
	</footer>
</div>

</div>
