<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
	if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
?>
		<!-- SCRIPT | JQUERY -->
		<script type="text/javascript" src="./js/actionjquery/tipos-veiculos.js"></script>
		<script type="text/javascript" src="./js/actionjquery/veiculo-tipo.js"></script>
		<script type="text/javascript" src="./js/actionjquery/estados-cidades.js"></script>

<div class="clearfix container">
<div class="row">
  <div class="col">
	<section class="card">
	<header class="title fundo-deep-purble">Inserir Dados</header>
		<article class="fundo-card" ng-controller="FormRegistro">
		  <form class="float-label" spellcheck="false" ng-submit="reg_veiculo()">
			<legend>Create Vehicle</legend>

			  <div class="control med">

<?php 

header('Content-Type:text/html;charset=UTF-8');

	include("../../../class/conexao.class.php");

	$sql = "SELECT id_usuario,user_nome FROM `USUARIO`";

	$usuariosdb = $conn->query($sql);

	if ($usuariosdb->num_rows <= 0) {
		echo ("" . $conn->error);
	} else {

			echo ('<select name="usuario" id="usuario" ng-model="usuario" required>');
			echo ('<option value="" disabled selected># - Usuário</option>');
		while($linha = $usuariosdb->fetch_assoc()) {
			echo ('<option value="' . $linha['id_usuario'] . '">' . $linha['user_nome'] . '</option>');
		}
			echo "</select>";
	}

	$conn->close();
?>
				<label for="usuario">Nome do Usuário</label>
			  </div>

			  <div class="control small">
				<select id="tipoauto" name="tipoauto" ng-model="tipoauto" required>
					<option value="" disabled selected># - Tipo de Veículo</option>
					<option value="CARRO">Carro</option>
					<option value="MOTO">Moto</option>
				</select>
				<label for="tipoauto">Tipo de Veículo</label>
			  </div>

			  <div class="control medium">
				<input type="text" name="placa" ng-model="placa" placeholder="Placa do Veículo" required/>
				<label for="placa">Placa do Veículo</label>
			  </div>

			  <div class="control small">
				<select id="fabricante" name="fabricante" required>
					<option value="" disabled selected>Fabricante</option>
				</select>
				<label for="fabricante">Fabricante</label>
			  </div>

			  <div class="control small">
				<select id="estado" name="estado">
					<option value="" disabled selected># - Estado</option>
					<option value=""></option>
					<option value="AC">Acre</option>
					<option value="AL">Alagoas</option>
					<option value="AM">Amazonas</option>
					<option value="AP">Amapá</option>
					<option value="BA">Bahia</option>
					<option value="CE">Ceará</option>
					<option value="DF">Distrito Federal</option>
					<option value="ES">Espírito Santo</option>
					<option value="GO">Goiás</option>
					<option value="MA">Maranhão</option>
					<option value="MT">Mato Grosso</option>
					<option value="MS">Mato Grosso do Sul</option>
					<option value="MG">Minas Gerais</option>
					<option value="PA">Pará</option>
					<option value="PB">Paraíba</option>
					<option value="PR">Paraná</option>
					<option value="PE">Pernambuco</option>
					<option value="PI">Piauí</option>
					<option value="RJ">Rio de Janeiro</option>
					<option value="RN">Rio Grande do Norte</option>
					<option value="RO">Rondônia</option>
					<option value="RS">Rio Grande do Sul</option>
					<option value="RR">Roraima</option>
					<option value="SC">Santa Catarina</option>
					<option value="SE">Sergipe</option>
					<option value="SP">São Paulo</option>
					<option value="TO">Tocantins</option>
				</select>
				<label for="estado">Estado</label>
			  </div>

			  <div class="control med">
				<select id="cidade" name="cidade" ng-model="cidade">
					<option value="" disabled selected># - Cidade</option>
				</select>
				<label for="cidade">Cidade</label>
			  </div>

			  <div class="control small">
				<select id="tipoveiculo" name="tipoveiculo" ng-model="tipoveiculo" required>
					<option value="" disabled selected># - Modelo</option>
				</select>
				<label for="tipoveiculo">Modelo</label>
			  </div>

			  <div class="control medium">
				<input type="date" name="dt_entrada" ng-model="dt_entrada" placeholder="Data de Entrada" />
				<label for="dt_entrada">Data de Entrada</label>
			  </div>

			  <div class="control medium">
				<input type="date" name="dtdata" ng-model="dt_entrega" placeholder="Data de Entrega" />
				<label for="dt_entrega">Data de Entrega</label>
			  </div>

			  <div class="control">
				<input type="submit" name="veiculo" id="veiculo" value="Cadastrar Veículo" />
			  </div>

		  </form>
		</article>
		<footer class="footer-card wrapper fundo-indigo">
			<span>Copyright</span> <span>-| BetsCar V1.0 |-</span> <span>© 2015 | iCSS</span>
		</footer>
	</section>
  </div>
</div>
</div>