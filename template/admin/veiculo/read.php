<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
	if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
?>

		<!-- SCRIPT | JQUERY -->
		<script type="text/javascript" src="./js/actionjquery/tipos-veiculos.js"></script>
		<script type="text/javascript" src="./js/actionjquery/veiculo-tipo.js"></script>
		<script type="text/javascript" src="./js/actionjquery/estados-cidades.js"></script>

<div ng-controller="ModalView">
<div ng-controller="UserUpdate">

<div class="row">
  <div class="col">
	<section class="card">
	<?php if ($_SESSION["nv_acesso"] == 1) { ?>
	<header class="title fundo-blue1">Dados do Veículo</header>
	<?php } elseif ($_SESSION["nv_acesso"] == 2) { ?>
	<header class="title fundo-blue1">Veículos Cadastrados</header>
	<?php } ?>
		<article class="fundo-card fundo-branco">
			<table class="sticky-wrap" name="usuario">
<?php 

header('Content-Type:text/html;charset=UTF-8');

	include("../../../class/conexao.class.php");

	$nomesession = $_SESSION["nome"];

	if ($_SESSION["nv_acesso"] == 1) {
		$sql[0] = "SELECT * FROM `VEICULO` INNER JOIN `USUARIO` ON (VEICULO.id_usuario = USUARIO.id_usuario) INNER JOIN `CIDADES` ON (VEICULO.id_cidade = CIDADES.id_cidade)  INNER JOIN `MODELO` ON (VEICULO.id_modelo = MODELO.id_modelo) INNER JOIN `FABRICANTE` ON (MODELO.id_fabricante = FABRICANTE.id_fabricante) WHERE user_nome = '$nomesession' ";
	}
	elseif ($_SESSION["nv_acesso"] == 2) {
		$sql[0] = "SELECT * FROM `VEICULO` INNER JOIN `USUARIO` ON (VEICULO.id_usuario = USUARIO.id_usuario) INNER JOIN `CIDADES` ON (VEICULO.id_cidade = CIDADES.id_cidade)  INNER JOIN `MODELO` ON (VEICULO.id_modelo = MODELO.id_modelo) INNER JOIN `FABRICANTE` ON (MODELO.id_fabricante = FABRICANTE.id_fabricante)";
	}

	$usuarios = $conn->query($sql[0]);

	if ($usuarios->num_rows <= 0) {
		//echo ("[Protocolo] = #");
		echo ("" . $conn->error);
	} else {

		while($linha = $usuarios->fetch_assoc()) {
			echo "<thead>";
				echo "<tr>";
					echo "<th>ID Veículo</th>";
					echo "<th>Usuário</th>";
					echo "<th>Tipo do Veículo</th>";
					echo "<th>Placa</th>";
					echo "<th>Modelo</th>";
					echo "<th>Fabricante</th>";
					echo "<th>Local</th>";
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
					echo "<tr>";
						echo ('<td class="err">' . $linha['id_veiculo'] . '</td><td class="user-name">' . $linha['user_nome'] . '</td><td>' . $linha['vehicle_tipo'] . '</td>');
						echo ('<td>' . $linha['vehicle_placa'] . '</td><td>' . $linha['model_modelo'] . '</td><td>' . $linha['user_fabricante'] . '</td><td>' . $linha['city_cidade'] . ' - ' . $linha['city_estado'] . '</td>');
					echo "</tr>";
			echo "</tbody>";

			echo "<thead>";
				echo "<tr>";
					echo "<th>Ano</th>";
					echo "<th>Cor</th>";
					echo "<th>Data de Entrada</th>";
					echo "<th>Data de Entrega</th>";
					if ($_SESSION["nv_acesso"] == 1) { 		echo "<th>Opções</th>"; }
					elseif ($_SESSION["nv_acesso"] == 2) { 	echo "<th colspan='2'>Opções</th>"; }
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
					echo "<tr>";
						echo ('<td>' . $linha['model_fabricante'] . '</td><td>' . $linha['model_cor'] . '</td><td>' . $linha['vehicle_dt_entrada'] . '</td>');
						echo ('<td>' . $linha['vehicle_dt_entrega'] . '</td>');
						if ($_SESSION["nv_acesso"] == 1) { 		echo ('<td> <a class="link" ng-click="editveiculo($event);open()" data="'. $linha['id_veiculo'] .'">Editar</a> </td>' ); }
						elseif ($_SESSION["nv_acesso"] == 2) {
							echo ('<td> <a class="link" ng-click="editveiculo($event);open()" data="'. $linha['id_veiculo'] .'">Editar</a> </td>' );
							if ($nomesession == $linha['user_nome']) {
								echo ('<td> <a class="link" ng-click="deleteveiculo($event)" data="'. $linha['id_veiculo'] .'" disabled>Excluir</a>' . '</td>');
							} else {
								echo ('<td> <a class="link" ng-click="deleteveiculo($event)" data="'. $linha['id_veiculo'] .'">Excluir</a>' . '</td>');
							}
						}
					echo "</tr>";
			echo "</tbody>";
		}
	}

	$conn->close();
?>
			</table>
		</article>
		<footer class="footer-card wrapper fundo-indigo">
			<span>Copyright</span> <span>-| BetsCar V1.0 |-</span> <span>© 2015 | iCSS</span>
		</footer>
	</section>
  </div>
</div>

<div modal="showModal" close="cancel()">
	<header class="modal-header">
	  <h1 class="modal-header-title">Editar Dados (Digite mesmo se Não for mudar os Dados)</h1>
	</header>
	<section class="modal-body">
		<article class="modal-content" ng-controller="FormRegistro">
		  <form class="float-label" spellcheck="false" ng-submit="veiculo_edit()">
			<legend>Edit Vehicle ID <span>{{ddata.id_veiculo}}</span></legend>

			  <div class="control">
				<select name="usuario" id="usuario" ng-model="usuario" required>
					<option value="" disabled selected># - Usuário</option>');
					<option value="{{ddata.id_usuario}}">{{ddata.user_nome}}</option>
				</select>
				<label for="usuario">Usuário</label>
			  </div>

			  <div class="control small">
				<select id="tipoauto" name="tipoauto" ng-model="tipoauto" required>
					<option value="" disabled selected># - Tipo de Veículo</option>
					<option value="CARRO">Carro</option>
					<option value="MOTO">Moto</option>
				</select>
				<label for="tipoauto">Tipo de Veículo</label>
			  </div>

			  <div class="control medium">
				<input type="text" name="placa" ng-model="placa" placeholder="{{ddata.vehicle_placa}}" required/>
				<label for="placa">Placa do Veículo</label>
			  </div>

			  <div class="control small">
				<select id="fabricante" name="fabricante" required>
					<option value="" disabled selected># - Fabricante</option>
				</select>
				<label for="fabricante">Fabricante</label>
			  </div>

			  <div class="control small">
				<select id="estado" name="estado">
					<option value="" disabled selected># - Estado</option>
					<option value=""></option>
					<option value="AC">Acre</option>
					<option value="AL">Alagoas</option>
					<option value="AM">Amazonas</option>
					<option value="AP">Amapá</option>
					<option value="BA">Bahia</option>
					<option value="CE">Ceará</option>
					<option value="DF">Distrito Federal</option>
					<option value="ES">Espírito Santo</option>
					<option value="GO">Goiás</option>
					<option value="MA">Maranhão</option>
					<option value="MT">Mato Grosso</option>
					<option value="MS">Mato Grosso do Sul</option>
					<option value="MG">Minas Gerais</option>
					<option value="PA">Pará</option>
					<option value="PB">Paraíba</option>
					<option value="PR">Paraná</option>
					<option value="PE">Pernambuco</option>
					<option value="PI">Piauí</option>
					<option value="RJ">Rio de Janeiro</option>
					<option value="RN">Rio Grande do Norte</option>
					<option value="RO">Rondônia</option>
					<option value="RS">Rio Grande do Sul</option>
					<option value="RR">Roraima</option>
					<option value="SC">Santa Catarina</option>
					<option value="SE">Sergipe</option>
					<option value="SP">São Paulo</option>
					<option value="TO">Tocantins</option>
				</select>
				<label for="estado">Estado</label>
			  </div>

			  <div class="control med">
				<select id="cidade" name="cidade" ng-model="cidade">
					<option value="" disabled selected># - Cidade</option>
				</select>
				<label for="cidade">Cidade</label>
			  </div>

			  <div class="control small">
				<select id="tipoveiculo" name="tipoveiculo" ng-model="tipoveiculo" required>
					<option value="" disabled selected># - Modelo</option>
				</select>
				<label for="tipoveiculo">Modelo</label>
			  </div>

			  <div class="control medium">
				<input type="date" name="dt_entrada" ng-model="dt_entrada" placeholder="{{ddata.vehicle_dt_entrada}}" />
				<label for="dt_entrada">Data de Entrada</label>
			  </div>

			  <div class="control medium">
				<input type="date" name="dtdata" ng-model="dt_entrega" placeholder="{{ddata.vehicle_dt_entrega}}" />
				<label for="dt_entrega">Data de Entrega</label>
			  </div>

			  <div class="control medium">
				<input type="submit" name="login" id="login" value="Atualizar os Dados" ng-click="ok()" />
			  </div>

			  <div class="control medium">
				<input type="button" class="btn-default" value="Cancelar" ng-click="cancel()" />
			  </div>

		  </form>
		</article>
	</section>
	<footer class="modal-footer wrapper">
	  <code>O registro é obrigatório dados reais do Usuário.</code>
	</footer>
</div>

</div>
</div>