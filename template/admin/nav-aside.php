<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
	if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
?>

<aside id="navleft">
  <div class="logo-area">
	LOGO
  </div>
  <nav>
	<a ui-sref="#">Home</a>
	<div class="nav-collapsible">
	  <input type="checkbox" id="nav-collapsible-1">
	  <label for="nav-collapsible-1">Portfolio</label>
	  <div class="wrap">
		<a ui-sref="#">Art</a>
		<a ui-sref="#">Design</a>
		<a ui-sref="#">Print</a>
		<a ui-sref="#">Web</a>
	  </div>
	</div>
	<div class="nav-collapsible">
	  <input type="checkbox" id="nav-collapsible-2">
	  <label for="nav-collapsible-2">Blog</label>
	  <div class="wrap">
		<a ui-sref="#">Art</a>
		<a ui-sref="#">Design</a>
		<a ui-sref="#">Print</a>
		<a ui-sref="#">Web</a>
	  </div>
	</div>
	<a ui-sref="#">GitHub</a>
  </nav>
</aside>