<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/home">';
	}

	if (isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == true) {
		session_destroy();
		session_write_close();
		echo '<meta http-equiv="refresh" content="0;url=#/home">';
	}
?>