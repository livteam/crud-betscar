<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
	if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
?>

<nav class="row">
	<ul class="nav col">
	  <li><a ui-sref="index">Home</a></li>

<?php 
	if ($_SESSION["nv_acesso"] == 1) {
?>
	  <li class="drop">Usuário
		<ul>
		  <li><a ui-sref="cliente-read">Atualizar seus DADOS</a></li>
		</ul>
	  </li>
<?php 
	} else if ($_SESSION["nv_acesso"] == 2) {
?>
	  <li class="drop">Usuário
		<ul>
		  <li><a ui-sref="cliente-insert">Cadastrar TODOS</a></li>
		  <li><a ui-sref="cliente-read">Atualizar TODOS</a></li>
		</ul>
	  </li>
<?php 
	}
?>

<?php 
	if ($_SESSION["nv_acesso"] == 1) {
?>
	  <li class="drop">Veículo
		<ul>
		  <li><a ui-sref="veiculo-read">Visualizar o Veículo Cadastrado</a></li>
		</ul>
	  </li>
<?php 
	} else if ($_SESSION["nv_acesso"] == 2) {
?>
	  <li class="drop">Veículo
		<ul>
		  <li><a ui-sref="veiculo-insert">Cadastrar Veículos do Usuário</a></li>
		  <li><a ui-sref="veiculo-read">Atualizar TODOS os Veículos do Usuário</a></li>
		  <li><a ui-sref="#">Subir Imagem</a></li>
		</ul>
	  </li>
<?php 
	}
?>

<?php 
	if ($_SESSION["nv_acesso"] == 1) {
?>
	  <li class="drop">CheckList
		<ul>
		  <li><a ui-sref="checklist-read">Visualizar o CheckList Cadastrado</a></li>
		</ul>
	  </li>
<?php 
	} else if ($_SESSION["nv_acesso"] == 2) {
?>
	  <li class="drop">CheckList
		<ul>
		  <li><a ui-sref="checklist-insert">Cadastrar CheckList do Usuário</a></li>
		  <li><a ui-sref="checklist-read">Atualizar TODOS os CheckList do Usuário</a></li>
		</ul>
	  </li>
<?php 
	}
?>

	  <li>
	  		<input type="checkbox" id="responsive-nav" name="responsive-nav" />
			<label for="responsive-nav">☰</label>
	  </li>
	  <li class="float-right"><a onclick="location.href='#/logout'">Sair do Sistema</a></li>
	</ul>
</nav>