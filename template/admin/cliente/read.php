<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
	if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
?>

		<!-- SCRIPT | JQUERY -->
		<script type="text/javascript" src="./js/actionjquery/estados-cidades.js"></script>

<div ng-controller="ModalView">
<div ng-controller="UserUpdate">

<div class="row">
  <div class="col">
	<section class="card">
	<?php if ($_SESSION["nv_acesso"] == 1) { ?>
	<header class="title fundo-blue1">Seus Dados</header>
	<?php } elseif ($_SESSION["nv_acesso"] == 2) { ?>
	<header class="title fundo-blue1">Usuários Cadastradados</header>
	<?php } ?>
		<article class="fundo-card fundo-branco">
			<table class="sticky-wrap" name="usuario">
<?php 

header('Content-Type:text/html;charset=UTF-8');

	include("../../../class/conexao.class.php");

	$nomesession = $_SESSION["nome"];

	if ($_SESSION["nv_acesso"] == 1) {
		$sql[0] = "SELECT id_usuario,user_nome,user_cpf,user_cnh,user_rg,user_address,user_bairro,user_contato,user_dt_nascimento,user_nv_acesso,city_estado,city_cidade FROM `USUARIO` INNER JOIN `CIDADES` on (USUARIO.id_cidade = CIDADES.id_cidade) WHERE user_nome = '$nomesession' ";
	}
	elseif ($_SESSION["nv_acesso"] == 2) {
		$sql[0] = "SELECT id_usuario,user_nome,user_cpf,user_cnh,user_rg,user_address,user_bairro,user_contato,user_dt_nascimento,user_nv_acesso,city_estado,city_cidade FROM `USUARIO` INNER JOIN `CIDADES` on (USUARIO.id_cidade = CIDADES.id_cidade) ";
	}

	$usuarios = $conn->query($sql[0]);

	if ($usuarios->num_rows <= 0) {
		//echo ("[Protocolo] = #");
		echo ("" . $conn->error);
	} else {

		while($linha = $usuarios->fetch_assoc()) {
			echo "<thead>";
				echo "<tr>";
					echo "<th>ID</th>";
					echo "<th>Usuário</th>";
					echo "<th>CPF</th>";
					echo "<th>CNH</th>";
					echo "<th>RG</th>";
					echo "<th>Contato</th>";
					echo "<th>DT Aniversário</th>";
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
					echo "<tr>";
						echo ('<td class="err">' . $linha['id_usuario'] . '</td><td class="user-name">' . $linha['user_nome'] . '</td><td>' . $linha['user_cpf'] . '</td>');
						echo ('<td>' . $linha['user_cnh'] . '</td><td>' . $linha['user_rg'] . '</td><td>' . $linha['user_contato'] . '</td><td>' . $linha['user_dt_nascimento'] . '</td>');
					echo "</tr>";
			echo "</tbody>";

			echo "<thead>";
				echo "<tr>";
					echo "<th>Endereço</th>";
					echo "<th>Bairro</th>";
					echo "<th>Nível de Acesso</th>";
					echo "<th>Local</th>";
					if ($_SESSION["nv_acesso"] == 1) { 		echo "<th>Opções</th>"; }
					elseif ($_SESSION["nv_acesso"] == 2) { 	echo "<th colspan='2'>Opções</th>"; }
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
					echo "<tr>";
						echo ('<td>' . $linha['user_address'] . '</td><td>' . $linha['user_bairro'] . '</td><td>' . $linha['user_nv_acesso'] . '</td>');
						echo ('<td>' . $linha['city_estado'] . ' - ' . $linha['city_cidade'] . '</td>');
						if ($_SESSION["nv_acesso"] == 1) { 		echo ('<td> <a class="link" ng-click="edituser($event);open()" data="'. $linha['id_usuario'] .'">Editar</a> </td>' ); }
						elseif ($_SESSION["nv_acesso"] == 2) {
							echo ('<td> <a class="link" ng-click="edituser($event);open()" data="'. $linha['id_usuario'] .'">Editar</a> </td>' );
							if ($nomesession == $linha['user_nome']) {
								echo ('<td> <a class="link" ng-click="deleteuser($event)" data="'. $linha['id_usuario'] .'" disabled>Excluir</a>' . '</td>');
							} else {
								echo ('<td> <a class="link" ng-click="deleteuser($event)" data="'. $linha['id_usuario'] .'">Excluir</a>' . '</td>');
							}
						}
					echo "</tr>";
			echo "</tbody>";
		}
	}

	$conn->close();
?>
			</table>
		</article>
		<footer class="footer-card wrapper fundo-indigo">
			<span>Copyright</span> <span>-| BetsCar V1.0 |-</span> <span>© 2015 | iCSS</span>
		</footer>
	</section>
  </div>
</div>

<div modal="showModal" close="cancel()">
	<header class="modal-header">
	  <h1 class="modal-header-title">Editar Dados (Digite mesmo se Não for mudar os Dados)</h1>
	</header>
	<section class="modal-body">
		<article class="modal-content" ng-controller="FormRegistro">
		  <form class="float-label" spellcheck="false" ng-submit="credenciais_edit()">
			<legend>Edit User ID <span>{{ddata.id_usuario}}</span></legend>

			  <div class="control">
				<input type="text" name="usuario" ng-model="usuario" placeholder="{{ddata.user_nome}}" required />
				<label for="usuario">Nome do Usuário</label>
			  </div>

			  <div class="control medium">
				<input type="number" name="cpf" ng-model="cpf" placeholder="{{ddata.user_cpf}}" pattern="[0-9]+$" required />
				<label for="cpf">CPF</label>
			  </div>

			  <div class="control medium">
				<input type="password" name="senha" ng-model="senha" placeholder="Senha" required />
				<label for="location">Senha</label>
			  </div>

			  <div class="control med">
				<input type="number" name="cnh" ng-model="cnh" placeholder="{{ddata.user_cnh}}" pattern="[0-9]+$" required />
				<label for="cnh">CNH</label>
			  </div>

			  <div class="control small">
				<input type="number" name="nvacesso" ng-model="nvacesso" placeholder="{{ddata.user_nv_acesso}}" required <?php if ($_SESSION["nv_acesso"] == 1) { ?> disabled <?php } ?> />
				<label for="nvacesso">Nível de Acesso | Default</label>
			  </div>

			  <div class="control small">
				<input type="number" name="rg" ng-model="rg" placeholder="{{ddata.user_rg}}" pattern="[0-9]+$" required />
				<label for="rg">RG</label>
			  </div>

			  <div class="control small">
				<input type="number" name="contato" ng-model="contato" placeholder="{{ddata.user_contato}}" />
				<label for="contato">Contato</label>
			  </div>

			  <div class="control medium">
				<input type="date" name="dtdata" ng-model="dtdata" placeholder="{{ddata.user_dt_nascimento}}" />
				<label for="dtdata">Data de Nascimento</label>
			  </div>

			  <div class="control">
				<input type="text" name="address" ng-model="address" placeholder="{{ddata.user_address}}" />
				<label for="address">Endereço</label>
			  </div>

			  <div class="control small">
				<input type="text" name="bairro" ng-model="bairro" placeholder="{{ddata.user_bairro}}" />
				<label for="bairro">Bairro e/ou Setor</label>
			  </div>

			  <div class="control small">
				<select id="estado" name="estado">
					<option value="" disabled selected># - Estado</option>
					<option value=""></option>
					<option value="AC">Acre</option>
					<option value="AL">Alagoas</option>
					<option value="AM">Amazonas</option>
					<option value="AP">Amapá</option>
					<option value="BA">Bahia</option>
					<option value="CE">Ceará</option>
					<option value="DF">Distrito Federal</option>
					<option value="ES">Espírito Santo</option>
					<option value="GO">Goiás</option>
					<option value="MA">Maranhão</option>
					<option value="MT">Mato Grosso</option>
					<option value="MS">Mato Grosso do Sul</option>
					<option value="MG">Minas Gerais</option>
					<option value="PA">Pará</option>
					<option value="PB">Paraíba</option>
					<option value="PR">Paraná</option>
					<option value="PE">Pernambuco</option>
					<option value="PI">Piauí</option>
					<option value="RJ">Rio de Janeiro</option>
					<option value="RN">Rio Grande do Norte</option>
					<option value="RO">Rondônia</option>
					<option value="RS">Rio Grande do Sul</option>
					<option value="RR">Roraima</option>
					<option value="SC">Santa Catarina</option>
					<option value="SE">Sergipe</option>
					<option value="SP">São Paulo</option>
					<option value="TO">Tocantins</option>
				</select>
				<label for="estado">Estado</label>
			  </div>

			  <div class="control medium">
				<select id="cidade" name="cidade" ng-model="cidade">
					<option value="" disabled selected>Cidade</option>
				</select>
				<label for="cidade">Cidade</label>
			  </div>

			  <div class="control medium">
				<input type="submit" name="login" id="login" value="Atualizar os Dados" ng-click="ok()" />
			  </div>

			  <div class="control medium">
				<input type="button" class="btn-default" value="Cancelar" ng-click="cancel()" />
			  </div>

		  </form>
		</article>
	</section>
	<footer class="modal-footer wrapper">
	  <code>O registro é obrigatório dados reais do Usuário.</code>
	</footer>
</div>

</div>
</div>