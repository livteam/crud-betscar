<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
	if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
?>

<section class="content" style="margin: 5.5em auto 0; max-width: 640px;">
  <div class="icon-content"><img src="./img/admin-microsoft.png"></div>
  <article>
	<h1 i18n-content="incognitoTabHeading">Você entrou no cPanel da administração de dados do BetsCar</h1>
	<p>
		Ás paginas de navegação neste painel, contém dados passados para nossos funcionários e eles cadastrão neste banco.
		<strong> Todas</strong> informações são restritas para cada usuário. Só será aceito dados reais cadastrados ou alterados,
		<a class="link" ui-sref="#">Saiba mais sobre restrição deste local.</a>
    </p>
    <p>
		<strong>Esta navegação está seguro, os dados contidos neste local estão codificados graças ao sistema MariaDB junto com PHP e AngularJS (Protocolo de dados JSON).</strong>
    </p>
  </article>
</section>
