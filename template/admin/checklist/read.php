<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
	if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
?>

		<!-- SCRIPT | JQUERY -->
		<script type="text/javascript" src="./js/actionjquery/veiculo-usuario.js"></script>

<div ng-controller="ModalView">
<div ng-controller="UserUpdate">

<div class="row">
  <div class="col">
	<section class="card">
	<?php if ($_SESSION["nv_acesso"] == 1) { ?>
	<header class="title fundo-blue1">Dados do Checklist</header>
	<?php } elseif ($_SESSION["nv_acesso"] == 2) { ?>
	<header class="title fundo-blue1">Checklist Cadastrados</header>
	<?php } ?>
		<article class="fundo-card fundo-branco">
			<table class="sticky-wrap" name="usuario">
<?php 

header('Content-Type:text/html;charset=UTF-8');

	include("../../../class/conexao.class.php");

	$nomesession = $_SESSION["nome"];

	if ($_SESSION["nv_acesso"] == 1) {
		$sql[0] = "SELECT * FROM `CHECKLIST` INNER JOIN `USUARIO` ON (CHECKLIST.id_usuario = USUARIO.id_usuario) INNER JOIN `VEICULO` ON (CHECKLIST.id_veiculo = VEICULO.id_veiculo) INNER JOIN `CIDADES` ON (VEICULO.id_cidade = CIDADES.id_cidade) INNER JOIN `ITEM` ON (CHECKLIST.id_item = ITEM.id_item) INNER JOIN `MODELO` ON (VEICULO.id_modelo = MODELO.id_modelo) INNER JOIN `FABRICANTE` ON (MODELO.id_fabricante = FABRICANTE.id_fabricante) WHERE user_nome = '$nomesession' ";
	}
	elseif ($_SESSION["nv_acesso"] == 2) {
		$sql[0] = "SELECT * FROM `CHECKLIST` INNER JOIN `USUARIO` ON (CHECKLIST.id_usuario = USUARIO.id_usuario) INNER JOIN `VEICULO` ON (CHECKLIST.id_veiculo = VEICULO.id_veiculo) INNER JOIN `CIDADES` ON (VEICULO.id_cidade = CIDADES.id_cidade) INNER JOIN `ITEM` ON (CHECKLIST.id_item = ITEM.id_item) INNER JOIN `MODELO` ON (VEICULO.id_modelo = MODELO.id_modelo) INNER JOIN `FABRICANTE` ON (MODELO.id_fabricante = FABRICANTE.id_fabricante)";
	}

	$usuarios = $conn->query($sql[0]);

	if ($usuarios->num_rows <= 0) {
		//echo ("[Protocolo] = #");
		echo ("" . $conn->error);
	} else {

		while($linha = $usuarios->fetch_assoc()) {
			echo "<thead>";
				echo "<tr>";
					echo "<th>ID Checklist</th>";
					echo "<th>Usuário</th>";
					echo "<th>Tipo do Veículo</th>";
					echo "<th>Placa</th>";
					echo "<th>Modelo</th>";
					echo "<th>Fabricante</th>";
					echo "<th>Local</th>";
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
					echo "<tr>";
						echo ('<td class="err">' . $linha['id_checklist'] . '</td><td class="user-name">' . $linha['user_nome'] . '</td><td>' . $linha['vehicle_tipo'] . '</td>');
						echo ('<td>' . $linha['vehicle_placa'] . '</td><td>' . $linha['model_modelo'] . '</td><td>' . $linha['user_fabricante'] . '</td><td>' . $linha['city_cidade'] . ' - ' . $linha['city_estado'] . '</td>');
					echo "</tr>";
			echo "</tbody>";

			echo "<thead>";
				echo "<tr>";
					echo "<th>Item</th>";
					echo "<th>Condição</th>";
					echo "<th>Funcionamento</th>";
					echo "<th>Conservação</th>";
					echo "<th>Comentário</th>";
					if ($_SESSION["nv_acesso"] == 1) { 		echo "<th>Opções</th>"; }
					elseif ($_SESSION["nv_acesso"] == 2) { 	echo "<th colspan='2'>Opções</th>"; }
				echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
					echo "<tr>";
						echo('<td class="err">' . $linha['nome_item'] . '</td>');
						echo ('<td>' . $linha['check_condicao'] . '</td><td>' . $linha['check_funcionamento'] . '</td><td>' . $linha['check_conservacao'] . '</td>');
						echo ('<td>' . $linha['check_comment'] . '</td>');
						if ($_SESSION["nv_acesso"] == 1) { 		echo ('<td> <a class="link" ng-click="editveiculo($event);open()" data="'. $linha['id_checklist'] .'">Editar</a> </td>' ); }
						elseif ($_SESSION["nv_acesso"] == 2) {
							echo ('<td> <a class="link" ng-click="editchecklist($event);open()" data="'. $linha['id_checklist'] .'">Editar</a> </td>' );
							if ($nomesession == $linha['user_nome']) {
								echo ('<td> <a class="link" ng-click="deletechecklist($event)" data="'. $linha['id_checklist'] .'" disabled>Excluir</a>' . '</td>');
							} else {
								echo ('<td> <a class="link" ng-click="deletechecklist($event)" data="'. $linha['id_checklist'] .'">Excluir</a>' . '</td>');
							}
						}
					echo "</tr>";
			echo "</tbody>";
		}
	}

	$conn->close();
?>
			</table>
		</article>
		<footer class="footer-card wrapper fundo-indigo">
			<span>Copyright</span> <span>-| BetsCar V1.0 |-</span> <span>© 2015 | iCSS</span>
		</footer>
	</section>
  </div>
</div>

<div modal="showModal" close="cancel()">
	<header class="modal-header">
	  <h1 class="modal-header-title">Editar Dados (Digite mesmo se Não for mudar os Dados)</h1>
	</header>
	<section class="modal-body">
		<article class="modal-content" ng-controller="FormRegistro">
		  <form class="float-label" spellcheck="false" ng-submit="check_edit()">
			<legend>Edit CheckList ID <span>{{ddata.id_checklist}}</span></legend>

			  <div class="control med3">
				<select name="usuario" id="usuario" ng-model="usuario" required>
					<option value="" disabled selected>Usuário</option>');
					<option value="{{ddata.id_usuario}}">{{ddata.user_nome}}</option>
				</select>
				<label for="usuario">Usuário</label>
			  </div>

			  <div class="control med3">
				<select name="veluserid" id="veluserid" ng-model="veluserid" required>
					<option value="" disabled selected>Veículo do Usuário</option>');
					<option value="{{ddata.id_veiculo}}">{{ddata.model_modelo}}</option>
				</select>
				<label for="veluserid">Veículo do Usuário</label>
			  </div>

			  <div class="control med3">
				<select name="itemveiculo" id="itemveiculo" ng-model="itemveiculo" required>
					<option value="" disabled selected>Item do Veículo</option>');
					<option value="{{ddata.id_item}}">{{ddata.nome_item}}</option>
				</select>
				<label for="itemveiculo">Item do Veículo</label>
			  </div>

			  <div class="control med3">
				<select id="condicao" name="condicao" ng-model="condicao" required>
					<option value="" disabled selected># - {{ddata.check_condicao}}</option>
					<option value="Não Aplicável">Não Aplicável</option>
					<option value="Vencido">Vencido</option>
					<option value="Completar">Completar</option>
					<option value="Não Funciona">Não Funciona</option>
					<option value="Funcionando">Funcionando</option>
					<option value="No Nível">No Nível</option>
					<option value="Ok">Ok</option>
					<option value="Em Dia">Em Dia</option>
					<option value="Normal">Normal</option>
					<option value="Bom">Bom</option>
					<option value="Ruim">Ruim</option>
					<option value="Riscado">Riscado</option>
					<option value="Amassado">Amassado</option>
					<option value="Trincado">Trincado</option>
					<option value="Calibrado">Calibrado</option>
					<option value="Descalibrado">Descalibrado</option>
				</select>
				<label for="condicao">Condição</label>
			  </div>

			  <div class="control med3">
				<select id="funcionamento" name="funcionamento" ng-model="funcionamento" required>
					<option value="" disabled selected># - {{ddata.check_funcionamento}}</option>
					<option value="Não Aplicável">Não Aplicável</option>
					<option value="Vencido">Vencido</option>
					<option value="Completar">Completar</option>
					<option value="Não Funciona">Não Funciona</option>
					<option value="Funcionando">Funcionando</option>
					<option value="No Nível">No Nível</option>
					<option value="Ok">Ok</option>
					<option value="Em Dia">Em Dia</option>
					<option value="Normal">Normal</option>
					<option value="Bom">Bom</option>
					<option value="Ruim">Ruim</option>
					<option value="Riscado">Riscado</option>
					<option value="Amassado">Amassado</option>
					<option value="Trincado">Trincado</option>
					<option value="Calibrado">Calibrado</option>
					<option value="Descalibrado">Descalibrado</option>
				</select>
				<label for="funcionamento">Funcionamento</label>
			  </div>

			  <div class="control med3">
				<select id="conservacao" name="conservacao" ng-model="conservacao" required>
					<option value="" disabled selected># - {{ddata.check_conservacao}}</option>
					<option value="Não Aplicável">Não Aplicável</option>
					<option value="Vencido">Vencido</option>
					<option value="Completar">Completar</option>
					<option value="Não Funciona">Não Funciona</option>
					<option value="Funcionando">Funcionando</option>
					<option value="No Nível">No Nível</option>
					<option value="Ok">Ok</option>
					<option value="Em Dia">Em Dia</option>
					<option value="Normal">Normal</option>
					<option value="Bom">Bom</option>
					<option value="Ruim">Ruim</option>
					<option value="Riscado">Riscado</option>
					<option value="Amassado">Amassado</option>
					<option value="Trincado">Trincado</option>
					<option value="Calibrado">Calibrado</option>
					<option value="Descalibrado">Descalibrado</option>
				</select>
				<label for="conservacao">Conservação</label>
			  </div>

			  <div class="control">
				<textarea ng-model="comentario" id="comentario" name="comentario" placeholder="{{ddata.check_comment}}" rows="4"></textarea>
				<label for="comentario">Comentário do Item</label>
			  </div>

			  <div class="control medium">
				<input type="submit" name="login" id="login" value="Atualizar os Dados" ng-click="ok()" />
			  </div>

			  <div class="control medium">
				<input type="button" class="btn-default" value="Cancelar" ng-click="cancel()" />
			  </div>

		  </form>
		</article>
	</section>
	<footer class="modal-footer wrapper">
	  <code>O registro é obrigatório dados reais do Usuário.</code>
	</footer>
</div>

</div>
</div>