<?php 
	session_start();

	if (!$_SESSION["nome"] || !$_SESSION["senha"] || !$_SESSION["nv_acesso"]) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
	if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
		echo '<meta http-equiv="refresh" content="0;url=#/acesso">';
	}
?>
		<!-- SCRIPT | JQUERY -->
		<script type="text/javascript" src="./js/actionjquery/veiculo-usuario.js"></script>

<div class="clearfix container">
<div class="row">
  <div class="col">
	<section class="card">
	<header class="title fundo-deep-purble">Inserir Dados</header>
		<article class="fundo-card" ng-controller="FormRegistro">
		  <form class="float-label" spellcheck="false" ng-submit="reg_checklist()">
			<legend>Create CheckList</legend>

			  <div class="control medium">

<?php 

header('Content-Type:text/html;charset=UTF-8');

	include("../../../class/conexao.class.php");

	$sql[0] = "SELECT id_usuario,user_nome FROM `USUARIO`";

	$usuariovel = $conn->query($sql[0]);

	if ($usuariovel->num_rows <= 0) {
		echo ("" . $conn->error);
	} else {

			echo ('<select name="usuario" id="usuario" ng-model="usuario" required>');
			echo ('<option value="" disabled selected># - Usuário</option>');
		while($linha = $usuariovel->fetch_assoc()) {
			echo ('<option value="' . $linha['id_usuario'] . '">' . $linha['user_nome'] . '</option>');
		}
			echo "</select>";
	}
?>
				<label for="usuario">Nome do Usuário</label>
			  </div>

			  <div class="control small">
				<select id="veluserid" name="veluserid" ng-model="veluserid" required>
					<option value="" disabled selected># - Veículo do Usuário</option>
				</select>
				<label for="veluserid">Veículo do Usuário</label>
			  </div>

			  <div class="control small">
<?php 

	$sql[1] = "SELECT id_item, nome_item FROM `ITEM`";

	$itemvel = $conn->query($sql[1]) or die("" . $conn->error);

			echo ('<select name="itemveiculo" id="itemveiculo" ng-model="itemveiculo" required>');
			echo ('<option value="" disabled selected># - Item do Veículo</option>');
		while($row = $itemvel->fetch_assoc()) {
			echo ('<option value="' . $row['id_item'] . '">' . $row['nome_item'] . '</option>');
		}
			echo "</select>";

	$conn->close();
?>
				<label for="itemveiculo">Item do Veículo</label>
			  </div>

			  <div class="control med3">
				<select id="condicao" name="condicao" ng-model="condicao" required>
					<option value="" disabled selected># - Condição</option>
					<option value="Não Aplicável">Não Aplicável</option>
					<option value="Vencido">Vencido</option>
					<option value="Completar">Completar</option>
					<option value="Não Funciona">Não Funciona</option>
					<option value="Funcionando">Funcionando</option>
					<option value="No Nível">No Nível</option>
					<option value="Ok">Ok</option>
					<option value="Em Dia">Em Dia</option>
					<option value="Normal">Normal</option>
					<option value="Bom">Bom</option>
					<option value="Ruim">Ruim</option>
					<option value="Riscado">Riscado</option>
					<option value="Amassado">Amassado</option>
					<option value="Trincado">Trincado</option>
					<option value="Calibrado">Calibrado</option>
					<option value="Descalibrado">Descalibrado</option>
				</select>
				<label for="condicao">Condição</label>
			  </div>

			  <div class="control med3">
				<select id="funcionamento" name="funcionamento" ng-model="funcionamento" required>
					<option value="" disabled selected># - Funcionamento</option>
					<option value="Não Aplicável">Não Aplicável</option>
					<option value="Vencido">Vencido</option>
					<option value="Completar">Completar</option>
					<option value="Não Funciona">Não Funciona</option>
					<option value="Funcionando">Funcionando</option>
					<option value="No Nível">No Nível</option>
					<option value="Ok">Ok</option>
					<option value="Em Dia">Em Dia</option>
					<option value="Normal">Normal</option>
					<option value="Bom">Bom</option>
					<option value="Ruim">Ruim</option>
					<option value="Riscado">Riscado</option>
					<option value="Amassado">Amassado</option>
					<option value="Trincado">Trincado</option>
					<option value="Calibrado">Calibrado</option>
					<option value="Descalibrado">Descalibrado</option>
				</select>
				<label for="funcionamento">Funcionamento</label>
			  </div>

			  <div class="control med3">
				<select id="conservacao" name="conservacao" ng-model="conservacao" required>
					<option value="" disabled selected># - Conservação</option>
					<option value="Não Aplicável">Não Aplicável</option>
					<option value="Vencido">Vencido</option>
					<option value="Completar">Completar</option>
					<option value="Não Funciona">Não Funciona</option>
					<option value="Funcionando">Funcionando</option>
					<option value="No Nível">No Nível</option>
					<option value="Ok">Ok</option>
					<option value="Em Dia">Em Dia</option>
					<option value="Normal">Normal</option>
					<option value="Bom">Bom</option>
					<option value="Ruim">Ruim</option>
					<option value="Riscado">Riscado</option>
					<option value="Amassado">Amassado</option>
					<option value="Trincado">Trincado</option>
					<option value="Calibrado">Calibrado</option>
					<option value="Descalibrado">Descalibrado</option>
				</select>
				<label for="conservacao">Conservação</label>
			  </div>

			  <div class="control">
				<textarea ng-model="comentario" id="comentario" name="comentario" placeholder="Comentário do Item" rows="4"></textarea>
				<label for="comentario">Comentário do Item</label>
			  </div>

			  <div class="control">
				<input type="submit" name="checklist" id="checklist" value="Cadastrar Checklist" />
			  </div>

		  </form>
		</article>
		<footer class="footer-card wrapper fundo-indigo">
			<span>Copyright</span> <span>-| BetsCar V1.0 |-</span> <span>© 2015 | iCSS</span>
		</footer>
	</section>
  </div>
</div>
</div>