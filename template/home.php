<header class="row" id="header">
  <div class="col">
	<div class="image phone">
	  	<div class="inner">
	  		<img src="./img/camaro-car.jpg" alt="Camaro Car" />
	  	</div>
	</div>
  </div>
  <div class="col">
	<div class="list">
		<h1>BetsCar - v1.0</h1>
	  	<li><h5>Novo Conceito Inovador!</h5></li>
	</div>
  </div>
</header>

<section class="float-icon">
	<article class="col-icon">
		<div class="media">
			<div class="pull-left">
				<i class="icon-md"></i>
			</div>
			<div class="media-body">
				<h3 class="media-heading">Twitter</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat dolorum non, ab quisquam consequatur ullam necessitatibus in sit placeat aperiam!</p>
			</div>
		</div>
	</article>
	<article class="col-icon">
		<div class="media">
			<div class="pull-left">
				<i class="icon-md"></i>
			</div>
			<div class="media-body">
				<h3 class="media-heading">Facebook</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat dolorum non, ab quisquam consequatur ullam necessitatibus in sit placeat aperiam!</p>
			</div>
		</div>
	</article>
	<article class="col-icon">
		<div class="media">
			<div class="pull-left">
				<i class="icon-md"></i>
			</div>
			<div class="media-body">
				<h3 class="media-heading">Google+ Plus</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat dolorum non, ab quisquam consequatur ullam necessitatibus in sit placeat aperiam!</p>
			</div>
		</div>
	</article>
</section>
<section class="wrapper">
	<article class="md-10">
		<h3 class="text-center">Participe do nosso sistema baseado em novas ferramentas como AngularJS | MariaDB | DART | Sway.</h3>
	</article>
</section>
<section class="box fundo-branco">
	<article class="">
		<header class="title">Projeto Feito para você.</header>
		<section class="fundo-card">
			<img class="img float-left" src="./img/carro-police.jpg" alt="Car Police">
			<p>Desenhado para todos os dispositivos, fornecendo serviço em alta disponibilidade, estruturação e criativo.</p>
			<p class="text-justify">Intenção do projeto que ele seja facil de instalação (baseado em TAG`s) e dinâmico (Com Temas facil
			de criação e instalação), contém cálculos pré-pronto para várias telas, fora a facilidade de edição em casos
			específicos (Novas Telas, Novos Efeitos, Novas Ferramentas, ...)</p>
			<div class="list">
				<h1>Alta Disponibilidade</h1>
				<li>UtraMonkey - IPVSan</li>
	  			<li>JkMount</li>
	  			<li>Cluster</li>
			</div>
		</section>
		<footer class="footer-card"></footer>
	</article>
</section>

<footer id="footer">
<p>Responsive iCSS V1.0 - Código Aberto | Copyright @ 2015. <a class="link" ui-sref="home">iRedes Computer</a>. Todos os direitos reservados. | Licença <code>Apache 2.0</code> - <a class="link" ui-sref="home">GitHub</a></p>
</footer>
