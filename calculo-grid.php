<?php

if (!defined('PHP_EOL')) {
	switch (strtoupper(substr(PHP_OS, 0, 3))) {
		// Windows
		case 'WIN':
		define('PHP_EOL', "\r\n");
		define('PHP_EOL', "<br />");
		define('\r', "<br />");
		define('\n', "<br />");
		break;

		// Mac
		case 'DAR':
		define('PHP_EOL', "\r");
		define('PHP_EOL', "<br />");
		define('\r', "<br />");
		break;

		// Unix
		default:
		define('PHP_EOL', "\n");
		define('PHP_EOL', "<br />");
		define('\n', "<br />");
	}
}

$max = 1862;

	$md16 = $max * 100 * 0.01;
	$md15 = $max * 93.75 * 0.01;
	$md14 = $max * 87.5 * 0.01;
	$md13 = $max * 81.25 * 0.01;
	$md12 = $max * 75 * 0.01;
	$md11 = $max * 68.75 * 0.01;
	$md10 = $max * 62.5 * 0.01;
	$md9 = $max * 56.25 * 0.01;
	$md8 = $max * 50 * 0.01;
	$md7 = $max * 43.75 * 0.01;
	$md6 = $max * 37.5 * 0.01;
	$md5 = $max * 31.25 * 0.01;
	$md4 = $max * 25 * 0.01;
	$md3 = $max * 18.75 * 0.01;
	$md2 = $max * 12.5 * 0.01;
	$md1 = $max * 6.25 * 0.01;

	echo ("	.md-16 { width: ". $md16 . "px 	} 	/* 100% */ <br />" .PHP_EOL);
	echo ("	.md-15 { width: ". $md15 . "px 	} 	/* 93.75% */ <br />" .PHP_EOL);
	echo ("	.md-14 { width: ". $md14 . "px 	} 	/* 87.5% */ <br />" .PHP_EOL);
	echo ("	.md-13 { width: ". $md13 . "px 	} 	/* 81.25% */ <br />" .PHP_EOL);
	echo ("	.md-12 { width: ". $md12 . "px 	} 	/* 75% */ <br />" .PHP_EOL);
	echo ("	.md-11 { width: ". $md11 . "px 	} 	/* 68.75% */ <br />" .PHP_EOL);
	echo ("	.md-10 { width: ". $md10 . "px 	} 	/* 62.5% */ <br />" .PHP_EOL);
	echo ("	.md-9 { width: ". $md9 . "px 	} 	/* 56.25% */ <br />" .PHP_EOL);
	echo ("	.md-8 { width: ". $md8 . "px 	} 	/* 50% */ <br />" .PHP_EOL);
	echo ("	.md-7 { width: ". $md7 . "px 	} 	/* 43.75% */ <br />" .PHP_EOL);
	echo ("	.md-6 { width: ". $md6 . "px 	} 	/* 37.5% */ <br />" .PHP_EOL);
	echo ("	.md-5 { width: ". $md5 . "px 	} 	/* 31.25% */ <br />" .PHP_EOL);
	echo ("	.md-4 { width: ". $md4 . "px 	} 	/* 25% */ <br />" .PHP_EOL);
	echo ("	.md-3 { width: ". $md3 . "px 	} 	/* 18.75% */ <br />" .PHP_EOL);
	echo ("	.md-2 { width: ". $md2 . "px 	} 	/* 12.5% */ <br />" .PHP_EOL);
	echo ("	.md-1 { width: ". $md1 . "px 	} 	/* 6.25% */ <br />" .PHP_EOL);

?>