Bem vindo ao BetsCar!
===================

Este projeto tem a finalidade de um CRUD simples para um sistema de carros baseado na atividade da **Faculdade SENAI FATESG** chamado **RentCar**. Mas decidimos propor o nome de **BetsCar** e neste sistema é baseado em Linguagem de Script **PHP5**.

Projeto Realizado em 24 de Junho de 2016.

----------





[TOC]

----------

Documentação
-------------

Há 10 anos no mercado de locação de carros, possui atualmente em seu negócio, processos executados de forma manual, ou seja, um cliente deve comparecer na sede da empresa para preencher um formulário com seus dados e assinar u, contrato de locação de veículos especificando qual o veículo desejado.

As reservar são realizadas via telefone, mas para isso o cliente já deve ter um formulário preenchido na empresa. A empresa possui um catálogo contendo todos os veículos existentes na mesma onde o cliente pode escolher o veículo desejado para locação.

Melhorar seu negócio permitindo com que seus clientes realizem a locação ou reserva sem necessidade de comparecer a sede ou ligar para a mesma. 

***Rent Car*** sistema Web de controle e locação de veículos visando à automatização dos processos de locação e reserva de automóveis através da internet.

> **Notas:**

> - O sistema deve permitir a manutenção dos dados dos clientes, bem como a manutenção dos dados dos veículos da sua propriedade.
> - O sistema deve permitir o controle das locações ou reservas de veículos.
> - O sistema não deve permitir a locação ou reserva de automóveis já reservados e/ou alocados para o período desejado.
> - O sistema não deve permitir a locação ou reserva de veículos cujo locatário possua CNH vencida.
> - O sistema não deve permitir a locação ou reservar de veículos cuja vistoria do automóvel esteja vencida.
> - O sistema deve alertar em caso de CNH vencidas e vistorias de veículos vencidas para que sejam tomadas as devidas providências para regularização.
> - Para liberação do veículo deve-se preencher um Check-List de liberação e dar entrada na liberação.
> - Na devolução do veículo deve-se preencher um Check-List de devolução e dar baixa na locação.

#### Instrução

Foi desenhado um **SGBD** no formato MER (*Modelo Entidade Relacionamento*).

![enter image description here](https://lh3.googleusercontent.com/-g4unWczki_s/V21jymyYgFI/AAAAAAAABSE/5BodTCctt-QFdN2HHy7oUPeaUCurVj_TwCKgB/s0/mer-betscar.PNG "mer-betscar.PNG")

#### Escolha das Ferramentas

Neste Projeto usamos o **PHP5** para consultar dados no Banco de Dados e o **AngularJS** para tratar às requisições dos dados mas também em algumas consultas rápidas foi utilizado o **jQuery** como também para alguns efeitos.

Ideia era de colocar o **Hack** do Facebook mas ainda sua documentação e implantação é complicada para instalar em servidores Linux baseado em Debian como por exemplo o **Ubuntu** e então ficamos com o *PHP* pelo suporte da comunidade.

Para o *Servidor Web* foi instalado a versão do **PHP5** e o Módulo **Proxy Balancer** para imagens apontando outros servidores de **Cluster**.

Na performance usamos o conceito de LVS (*Linux Virtual Server*) como auxilio do IP Virtual.

#### Construção da Infraestrutura

Foi utilizado o **VirtualBox** para montagem dos Servidores e em cada máquina tem os serviços separados:

    1. **SV DHCP-DNS:** Contendo configurações básica para tratanebti de nomes por IP;
    2. **SV Web:** Apache Web com Proxy Balancer, WebDAV (Substituir o Protocolo FTP) e o Workers List do AJP;
    3. **SV Cluster:** NGINX e LIGHTTPD com imagens (Pasta **IMG**);
    4. **SV Aplication:** Tomcat somente com uma imagem para usar a porta AJP (Pasta **IMG**);
    5. **SV SGBD:** MySQL;
    6. **SV IP-Virtual**: Keepalived + IPVSADM.

> **Notas:** Todos os servidores estão sendo adicionados aos **containers Docker**, pode seguir o desenvolvimento [clicando aqui](https://hub.docker.com/r/ileonardo/).

### Requisitos

Foi utilizado a versão **Release** de cada aplicação de acordo com a ultima data do projeto no dia 24 de Junho de 2016.

> **Notas:**

> - [HTTPD](#HTTPD): v2.4.20
> - [APR](#APR): v1.5.2
> - [APR-UTIL](#APR-UTIL): v1.5.4
> - [APR-ICONV](#APR_ICONV): v1.2.1
> - ​

> **App Web:**

> - [AngularJS](#ANGULARJS): v1.4.6
> - [UIRouter](#UIROUTER): v0.2.15
> - [NG-Module](#NG_MODULE): v11.sep.2014
> - [State-Files](#STATE_FILES): v22.jul.2014
> - [JQuery](#JQUERY): v1.11.3

> **Obs.:**

> - Baseado no SO Debian/Ubuntu 14.04 LTS
> - [Browser](#BROWSER): Google Chrome (Blink), Mozilla (26 ou superior), Internet Explorer (10 ou superior)
> - [Apache](#APACHE): Tutorial completo da Compilação só [seguir o Google Drive aqui](https://drive.google.com/file/d/0B-kjC4c84rQfSF8tdzR3VFVSWUU/view?usp=sharing).

----------


Estrutura do Site
-------------------

```
/usr/local/apache2/htdocs/:.
├───class
├───css
│   ├───fonts
│   └───fonts-bold
├───img
├───js
│   ├───actionangular
│   └───actionjquery
├───php
└───template
    └───admin
        ├───checklist
        ├───cliente
        └───veiculo
```

#### Configuração do Site

Na pasta **class** tem um arquivo chamado **conexao.class.php** para apontar o Servidor de Banco de Dados:

    <?php
    	//Abrindo Conexão
    	define('_HOST_NAME','localhost');
    	define('_DATABASE_NAME','IREDESCOMPUTER');
    	//define('_HOST_NAME','localhost');
    	//define('_DATABASE_NAME','iredescomputer');
    	define('_DATABASE_USER_NAME','root');
    	define('_DATABASE_PASSWORD','adminadmin');
    	//define('_DATABASE_PASSWORD','');
     
    	$conn = new MySQLi(_HOST_NAME,_DATABASE_USER_NAME,_DATABASE_PASSWORD,_DATABASE_NAME);
    
    	if($conn->connect_errno) {
    		echo("[ERRO]: Verifica o CLASS da Conexão do Banco de Dados." . "\n");
    		die("[ERRO]: -> ". $conn->connect_error);
    	}
    
    	//UTF8
    	$conn->query("SET NAMES utf8");
    
    ?>

#### Configuração do Banco de Dados

Crie um **database** chamado **IREDESCOMPUTER** e poderá subir o arquivo [backup.sql](https://drive.google.com/open?id=0B-kjC4c84rQfU0EwZ29SSm5zeDA).

> **Notas:** Em algumas tabelas é utilizado os dados na **ID 0** por isso deve ter para funcionar o site porque o **phpMyAdmin** dependendo de sua versão rejeita esta configuração.

----------


Novas Utilidades
-------------

Quem tem o **Android** pode subir este site através de um Aplicativo chamado [Palapa Web Server](https://play.google.com/store/apps/details?id=com.alfanla.android.pws&hl=pt_BR) que está no Google Play.

#### Instalando

 - Baixe o **apk** através deste [link](https://drive.google.com/open?id=0B-kjC4c84rQfU0EwZ29SSm5zeDA) e instale (não necessita do aparelho rooteado);
 - Após Instalado, baixe o [phpMyAdmin](https://drive.google.com/open?id=0B-kjC4c84rQfUEtTVFpfQVJFak0) e coloque a pasta neste diretório:

```
Android:.
└───pws
    ├───logs
    ├───phpmyadmin
    └───www
        └───betscar
```

> **Notas:** Para o acesso do Palapa Web Server ele aplica algumas senhas e no aplicativo tem a opção de resetar mas para mim não funcionou. por isso segui o padrão de senhas neste site: http://alfanla.com/palapa-web-server/
>
> Document Root
> - Patch: **pws/www/**
>
> Acessar os Sites
> - link: **http://127.0.0.1:8080**
>
> Web Admin Informations (Tem que instalar via aplicativo)
> - link: **http://127.0.0.1:9999**
> - login: **admin**
> - senha: **admin**
>
> MySQL Informations (acesso ao SGBD)
> - IP: **127.0.0.1**
> - login: **root**
> - senha: **adminadmin**
>
> phpMyAdmin
> - Link: **http://127.0.0.1:9999/phpmyadmin**
> - login: **root**
> - senha: **adminadmin**

----------

## Direitos autorais e Licença

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **MIT**](https://opensource.org/licenses/MIT).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
[^Palapa Web Server App]: Foi utilizado esse app para simular um servidor em Aparelhos Android, link do proprietário [Clique aqui](http://alfanla.com/palapa-web-server/).
[^httpd]: A instalação Source das dependências foram baseadas no site apontado pelo [apache](http://ftp.unicamp.br/pub/apache/).
[^Projeto]: Todos os anexos e a documentação da Atividade original só [clique aqui](https://drive.google.com/open?id=0B-kjC4c84rQfcXV6X3BrUXBfWXM).