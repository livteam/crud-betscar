$(function() {

	$("#fabricante").on('change', function() {

		var fabricante = $("#fabricante").val();

		$.post("./php/fabricante.php", {
			fabricante: fabricante

		}, function( data ) { 
			$("select[name=tipoveiculo]").html(data);
		});
	})
});