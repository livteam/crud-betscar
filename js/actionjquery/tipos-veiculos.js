$(function() {

	$("#tipoauto").on('change', function() {

		var tipoauto = $("#tipoauto").val();

		$.post("./php/tipoauto.php", {
			tipoauto: tipoauto

		}, function( data ) { 
			$("select[name=fabricante]").html(data);
		});
	})
});