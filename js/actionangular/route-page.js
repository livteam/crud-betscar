var routerApp = angular.module('routerApp', ['ui.router','stateFiles']);

routerApp.config(function($stateProvider, $urlRouterProvider) {

	// For any unmatched url, redirect to /home
	$urlRouterProvider.otherwise('/home');

	$stateProvider

	  // RAIZ
		.state('home', {
			url: '/home',
			cache: false,
			containerClass : 'clearfix container-fluid',
			views: {
				"title": { template: '<title>Betscar</title>' },
				"nav": { templateUrl: './template/nav.php' },
				"nav-aside": { templateUrl: './template/nav-aside.php' },
				"site": { templateUrl: './template/home.php' }
			}
		})
		.state('service', {
			url: '/service',
			containerClass : 'clearfix container-fluid',
			views: {
				"title": { template: '<title>Betscar | Serviços</title>' },
				"nav": { templateUrl: './template/nav.php' },
				"nav-aside": { templateUrl: './template/nav-aside.php' },
				"site": { templateUrl: './template/service.php' }
			}
		})
		.state('acesso', {
			url: '/acesso',
			containerClass : 'clearfix wrapper',
			views: {
				"title": { template: '<title>Acesso</title>' },
				"site": { templateUrl: './template/acesso.php' },
			},
			//css: ['/css/icss.css','/css/grid.css']
			//js: './js/actionjquery/estados-cidades.js'
		})

		  // ADMIN
			.state('index', {
				url: '/index',
				containerClass : 'clearfix container-fluid',
				views: {
					"title": { template: '<title>ADMIN | Betscar</title>' },
					"nav": { templateUrl: './template/admin/nav.php' },
					"nav-aside": { templateUrl: './template/admin/nav-aside.php' },
					"site": { templateUrl: './template/admin/index.php' }
				}
			})
			.state('logout', {
				url: '/logout',
				containerClass : 'clearfix container-fluid',
				views: {
					"title": { template: '<title>ADMIN | Betscar</title>' },
					"site": { templateUrl: './template/admin/logout.php' }
				}
			})
			  // ADMIN | Cliente
				.state('cliente-insert', {
					url: '/cliente-insert',
					containerClass : 'clearfix container-fluid background1',
					views: {
						"title": { template: '<title>ADMIN - Cliente Insert | Betscar</title>' },
						"nav": { templateUrl: './template/admin/nav.php' },
						"nav-aside": { templateUrl: './template/admin/nav-aside.php' },
						"site": { templateUrl: './template/admin/cliente/insert.php' }
					}
				})
				.state('cliente-read', {
					url: '/cliente-read',
					controller : 'ClienteRead',
					containerClass : 'clearfix container-fluid background1',
					views: {
						"title": { template: '<title>ADMIN - Cliente Read | Betscar</title>' },
						"nav": { templateUrl: './template/admin/nav.php' },
						"nav-aside": { templateUrl: './template/admin/nav-aside.php' },
						"site": { templateUrl: './template/admin/cliente/read.php' }
					}
				})
			  // ADMIN | Veiculo
				.state('veiculo-insert', {
					url: '/veiculo-insert',
					containerClass : 'clearfix container-fluid background1',
					views: {
						"title": { template: '<title>ADMIN - Veiculo Insert | Betscar</title>' },
						"nav": { templateUrl: './template/admin/nav.php' },
						"nav-aside": { templateUrl: './template/admin/nav-aside.php' },
						"site": { templateUrl: './template/admin/veiculo/insert.php' }
					}
				})
				.state('veiculo-read', {
					url: '/veiculo-read',
					controller : 'ClienteRead',
					containerClass : 'clearfix container-fluid background1',
					views: {
						"title": { template: '<title>ADMIN - Veiculo Read | Betscar</title>' },
						"nav": { templateUrl: './template/admin/nav.php' },
						"nav-aside": { templateUrl: './template/admin/nav-aside.php' },
						"site": { templateUrl: './template/admin/veiculo/read.php' }
					}
				})
			  // ADMIN | CheckList
				.state('checklist-insert', {
					url: '/checklist-insert',
					containerClass : 'clearfix container-fluid background1',
					views: {
						"title": { template: '<title>ADMIN - CheckList Insert | Betscar</title>' },
						"nav": { templateUrl: './template/admin/nav.php' },
						"nav-aside": { templateUrl: './template/admin/nav-aside.php' },
						"site": { templateUrl: './template/admin/checklist/insert.php' }
					}
				})
				.state('checklist-read', {
					url: '/checklist-read',
					controller : 'ClienteRead',
					containerClass : 'clearfix container-fluid background1',
					views: {
						"title": { template: '<title>ADMIN - CheckList Read | Betscar</title>' },
						"nav": { templateUrl: './template/admin/nav.php' },
						"nav-aside": { templateUrl: './template/admin/nav-aside.php' },
						"site": { templateUrl: './template/admin/checklist/read.php' }
					}
				});
});

routerApp.run(function($rootScope) {

	$rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
		$rootScope.containerClass = toState.containerClass;
	});

});

routerApp.controller('ClienteRead', ['$scope', '$http', function($scope, $http) {

	$scope.credenciais_reg = function () {

	if($scope.contato === undefined) $scope.contato = null; if($scope.dtdata === undefined) $scope.dtdata = null;
	if($scope.address === undefined) $scope.address = null; if($scope.bairro === undefined) $scope.bairro = null;
	if($scope.cidade === undefined) $scope.cidade = null;

		var formData = { 
			'usuario' : $scope.usuario, 'cpf' : $scope.cpf, 'senha' : $scope.senha, 'cnh' : $scope.cnh,
			'rg' : $scope.rg, 'contato' : $scope.contato, 'dtdata' : $scope.dtdata,
			'address' : $scope.address, 'bairro' : $scope.bairro, 'cidade' : $scope.cidade
		};

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/registro-conect.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			console.log(status + ' - ' + data);
			  if ( data.trim() === '[Protocolo] = #3') {
				alert('[ERRO]: Não foi possível criar o Cadastro.\n# - Usuário ou CPF ou CNH ou RG já cadastrados.');
			  }
			  if ( data.trim() === '[Protocolo] = #4') {
				alert('[ERRO]: Não foi possível gravar os dados no Banco de Dados (FATAL).\n# - Contate imediatamente o Administrador do DB!');
			  }
			  if ( data.trim() === '[Protocolo] = #5') {
				alert('[INFO]: Usuário cadastrado com sucesso.\n# - Nome: ' + $scope.usuario + ', Já pode conectar no sistema.');
			  }
		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});
	}
}]);