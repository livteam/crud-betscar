var app = angular.module("ModalWindow", ["modal.window"]);

app.controller('ModalView', function ($scope,$sce) {

	$scope.open = function(obj) {
		$scope.showModal = true;
		//var modal = (obj.target.attributes.data.value);
		//$scope.myHTML = $sce.trustAsHtml(modal);
	};

	$scope.ok = function() {
		$scope.showModal = false;
	};

	$scope.cancel = function() {
		$scope.showModal = false;
	};
});

app = angular.module('modal.window', []).constant('modalConfig', {
	backdrop: true,
	escape: true
})

app.directive('modal', ['$parse', 'modalConfig', function($parse, modalConfig) {
	var backdropEl;
	var body = angular.element(document.getElementsByTagName('body')[0]);

	return {
		restrict: 'EA',
		link: function(scope, elm, attrs) {
			var opts = angular.extend({}, modalConfig, scope.$eval(attrs.uiOptions || attrs.bsOptions || attrs.options));
			var shownExpr = attrs.modal || attrs.show;
			var setClosed;

			if (attrs.close) {
				setClosed = function() {
					scope.$apply(attrs.close);
				};
			} else {
				setClosed = function() {
					scope.$apply(function() {
						$parse(shownExpr).assign(scope, false);
					});
				};
			}

			elm.addClass('modal');

			if (opts.backdrop && !backdropEl) {
				backdropEl = angular.element('<div class="modal-backdrop"></div>');
				backdropEl.css('display','none');
				body.append(backdropEl);
			}

			function setShown(shown) {
				scope.$apply(function() {
					model.assign(scope, shown);
				});
			}

			function escapeClose(evt) {
				if (evt.which === 27) { setClosed(); }
	      	}

			function clickClose() {
				setClosed();
			}

			function close() {
				if (opts.escape) { body.unbind('keyup', escapeClose); }
				if (opts.backdrop) {
					backdropEl.css('display', 'none').removeClass('in');
					backdropEl.unbind('click', clickClose);
				}
				elm.css('display', 'none').removeClass('in');
				body.removeClass('modal-open');
			}

			function open() {
				if (opts.escape) { body.bind('keyup', escapeClose); }
				if (opts.backdrop) {
					backdropEl.css('display', 'block').addClass('in');

					if(opts.backdrop != "static") {
						backdropEl.bind('click', clickClose);
					}
				}
				elm.css('display', 'block').addClass('in md-8');

				body.addClass('modal-open');
			}

			scope.$watch(shownExpr, function(isShown, oldShown) {
				if (isShown) {
					open();
				} else {
					close();
				}
			});
		}
	};
}]);