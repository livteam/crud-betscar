var Myapp = angular.module('Acesso',[]);

Myapp.controller('FormLogin', ['$scope', '$http', function($scope, $http) {

	$scope.credenciais = function () {

		var formData = { 'usuario' : $scope.usuario, 'cpf' : $scope.cpf, 'senha' : $scope.senha };
		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/conexao.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + data);
			  if ( data.trim() === '[Protocolo] = #1') {
				alert("[ERRO]: Login ou CPF ou Senha Incorretos.\n# - Verifique os dados inseridos.");
			  }
			  if ( data.trim() === '[Protocolo] = #2') {
				if (confirm("[INFO] - Autenticado com sucesso.\n# - Clique em OK para ser Redirecionado ao Painel Administrativo.") == true) {
					window.location.href = "#/index";
				} else { 
					alert("# - Cancelado com sucesso!");
					window.location.href = "#/logout";
				}
			  }
		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});
	}
}]);

Myapp.controller('FormRegistro', ['$scope', '$http', function($scope, $http) {

	$scope.credenciais_reg = function () {

	if($scope.contato === undefined) $scope.contato = null; if($scope.dtdata === undefined) $scope.dtdata = null;
	if($scope.address === undefined) $scope.address = null; if($scope.bairro === undefined) $scope.bairro = null;
	if($scope.cidade === undefined) $scope.cidade = null; if($scope.nvacesso === undefined) $scope.nvacesso = 1;

		var formData = { 
			'usuario' : $scope.usuario, 'cpf' : $scope.cpf, 'senha' : $scope.senha, 'cnh' : $scope.cnh,
			'rg' : $scope.rg, 'contato' : $scope.contato, 'dtdata' : $scope.dtdata, 'address' : $scope.address,
			'bairro' : $scope.bairro, 'cidade' : $scope.cidade, 'nvacesso' : $scope.nvacesso
		};

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/registro-conect.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + data);
			  if ( data.trim() === '[Protocolo] = #3') {
				alert('[ERRO]: Não foi possível criar o Cadastro.\n# - Usuário ou CPF ou CNH ou RG já cadastrados.');
			  }
			  if ( data.trim() === '[Protocolo] = #4') {
				alert('[ERRO]: Não foi possível gravar os dados no Banco de Dados (FATAL).\n# - Contate imediatamente o Administrador do DB!');
			  }
			  if ( data.trim() === '[Protocolo] = #5') {
				alert('[INFO]: Usuário cadastrado com sucesso.\n# - Nome: ' + $scope.usuario + ', Já pode conectar no sistema.');
				window.location.reload();
			  }
		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});
	}

	$scope.credenciais_edit = function () {

	if($scope.contato === undefined) $scope.contato = null; if($scope.dtdata === undefined) $scope.dtdata = null;
	if($scope.address === undefined) $scope.address = null; if($scope.bairro === undefined) $scope.bairro = null;
	if($scope.cidade === undefined) $scope.cidade = null; if($scope.nvacesso === undefined) $scope.nvacesso = 1;

		var formData = { 
			'usuario' : $scope.usuario, 'cpf' : $scope.cpf, 'senha' : $scope.senha, 'cnh' : $scope.cnh,
			'rg' : $scope.rg, 'contato' : $scope.contato, 'dtdata' : $scope.dtdata, 'address' : $scope.address,
			'bairro' : $scope.bairro, 'cidade' : $scope.cidade, 'nvacesso' : $scope.nvacesso
		};

		console.log(formData);

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/update-user.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + data);
			  if ( data.trim() === '[Protocolo] = #8') {
				alert('[ERRO]: Não foi possível atualizar o Cadastro (FATAL). \n# - Contate imediatamente o Administrador do DB! ');
			  }
			  if ( data.trim() === '[Protocolo] = #9') {
				alert('[INFO]: Usuário cadastrado com sucesso.\n# - Nome: ' + $scope.usuario + ', Já pode conectar no sistema.');
				window.location.reload();
			  }
		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});
	}

	$scope.veiculo_edit = function () {

	if($scope.dt_entrada === undefined) $scope.dt_entrada = null; if($scope.dt_entrega === undefined) $scope.dt_entrega = null;
	if($scope.cidade === undefined) $scope.cidade = null;

		var formData = { 
			'usuario' : $scope.usuario, 'placa' : $scope.placa, 'cidade' : $scope.cidade, 'tipoveiculo' : $scope.tipoveiculo,
			'dt_entrada' : $scope.dt_entrada, 'dt_entrega' : $scope.dt_entrega
		};

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/update-veiculo.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + data);
			  if ( data.trim() === '[Protocolo] = #14') {
				alert('[ERRO]: Não foi possível atualizar o Cadastro (FATAL). \n# - Contate imediatamente o Administrador do DB! ');
			  }
			  if ( data.trim() === '[Protocolo] = #15') {
				alert('[INFO]: Veículo atualizado com sucesso.\n# - Dados criptografados de acordo com o usuário do sistema.');
				window.location.reload();
			  }
		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});
	}

	$scope.check_edit = function () {

	if($scope.comentario === undefined) $scope.comentario = null;

		var formData = { 
			'usuario' : $scope.usuario, 'veluserid' : $scope.veluserid, 'itemveiculo' : $scope.itemveiculo,
			'condicao' : $scope.condicao, 'funcionamento' : $scope.funcionamento, 'conservacao' : $scope.conservacao,
			'comentario' : $scope.comentario
		};

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/update-checklist.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + data);
			  if ( data.trim() === '[Protocolo] = #20') {
				alert('[ERRO]: Não foi possível atualizar o Cadastro (FATAL). \n# - Contate imediatamente o Administrador do DB! ');
			  }
			  if ( data.trim() === '[Protocolo] = #21') {
				alert('[INFO]: Checklist atualizado com sucesso.\n# - Dados criptografados de acordo com o usuário do sistema.');
				window.location.reload();
			  }
		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});
	}

	$scope.reg_veiculo = function () {

	if($scope.dt_entrada === undefined) $scope.dt_entrada = null; if($scope.dt_entrega === undefined) $scope.dt_entrega = null;
	if($scope.cidade === undefined) $scope.cidade = null;

		var formData = { 
			'usuario' : $scope.usuario, 'placa' : $scope.placa, 'cidade' : $scope.cidade, 'tipoveiculo' : $scope.tipoveiculo,
			'dt_entrada' : $scope.dt_entrada, 'dt_entrega' : $scope.dt_entrega
		};

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/registro-veiculo.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + data);
			  if ( data.trim() === '[Protocolo] = #10') {
				alert('[ERRO]: Não foi possível cadastrar o Veículo (FATAL).\n# - Contate imediatamente o Administrador do DB!');
			  }
			  if ( data.trim() === '[Protocolo] = #11') {
				alert('[INFO]: Veículo Cadastrado com sucesso.\n# - Dados salvos nos dados do usuário.');
				window.location.reload();
			  }
		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});
	}

	$scope.reg_checklist = function () {

	if($scope.comentario === undefined) $scope.comentario = null;

		var formData = { 
			'usuario' : $scope.usuario, 'veluserid' : $scope.veluserid, 'itemveiculo' : $scope.itemveiculo,
			'condicao' : $scope.condicao, 'funcionamento' : $scope.funcionamento, 'conservacao' : $scope.conservacao,
			'comentario' : $scope.comentario
		};

		console.log(formData);

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/registro-checklist.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			console.log(status + ' - ' + data);
			  if ( data.trim() === '[Protocolo] = #16') {
				alert('[ERRO]: Não foi possível cadastrar o CheckList (FATAL).\n# - Contate imediatamente o Administrador do DB!');
			  }
			  if ( data.trim() === '[Protocolo] = #17') {
				alert('[INFO]: CheckList Cadastrado com sucesso.\n# - Dados salvos nos dados do usuário.');
				window.location.reload();
			  }
		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});
	}
	
}]);

Myapp.controller('UserUpdate', ['$scope', '$http', function($scope, $http) {

	$scope.edituser = function(obj) {

	var iduser = (obj.target.attributes.data.value);

	$scope.userid = iduser;

		var formData = { 
			'userid' : $scope.userid
		};

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/user-update.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + angular.toJson(data));
			var stringdata = angular.toJson(data);

			$scope.ddata = angular.fromJson(stringdata);

		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});


	}

	$scope.editveiculo = function(obj) {

	var veiculoid = (obj.target.attributes.data.value);

	$scope.veiculoid = veiculoid;

		var formData = { 
			'veiculoid' : $scope.veiculoid
		};

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/veiculo-update.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + data);
			var stringdata = angular.toJson(data);

			$scope.ddata = angular.fromJson(stringdata);

		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});


	}

	$scope.editchecklist = function(obj) {

	var checkid = (obj.target.attributes.data.value);

	$scope.checkid = checkid;

		var formData = { 
			'checkid' : $scope.checkid
		};

		var postData = 'myData='+JSON.stringify(formData);

		var request = $http({
			method: "POST",
			url: './php/checklist-update.php',
			data: postData,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		});

		request.success(function (data, status, headers, config) {
			$scope.success = true;
			//console.log(status + ' - ' + data);
			var stringdata = angular.toJson(data);

			$scope.ddata = angular.fromJson(stringdata);

		});
		request.error(function (data, status, headers, config) {
			$scope.error = true;
			console.log(error);
		});


	}

	$scope.deleteuser = function(obj) {

		var iduser = (obj.target.attributes.data.value);

		if (confirm("[INFO] - Delete User.\n# - Deseja Deletar a ID = " + iduser + "?") == true) {

			var formData = { 
				'iduser' : iduser
			};

			var postData = 'myData='+JSON.stringify(formData);

			var request = $http({
				method: "POST",
				url: './php/user-delete.php',
				data: postData,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});

			request.success(function (data, status, headers, config) {
				$scope.success = true;
				console.log(status + ' - ' + data);
				  if ( data.trim() === '[Protocolo] = #6') {
					alert('[ERRO]: Houve Problema ao deletar, contate o Administrador.\n# - Não foi possível deletar o Usuário.');
				  }
				  if ( data.trim() === '[Protocolo] = #7') {
					alert('[INFO]: Usuário deletado com sucesso.\n# - Poderá criar outro usuário com os mesmos dados agora.');
					window.location.reload();
				  }
			});
			request.error(function (data, status, headers, config) {
				$scope.error = true;
				console.log(error);
			});

		} else { 
			alert("# - Cancelado com sucesso!");
		}
	}

	$scope.deleteveiculo = function(obj) {

		var idveiculo = (obj.target.attributes.data.value);

		if (confirm("[INFO] - Delete Vehicle.\n# - Deseja Deletar a ID = " + idveiculo + "?") == true) {

			var formData = { 
				'idveiculo' : idveiculo
			};

			var postData = 'myData='+JSON.stringify(formData);

			var request = $http({
				method: "POST",
				url: './php/veiculo-delete.php',
				data: postData,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});

			request.success(function (data, status, headers, config) {
				$scope.success = true;
				//console.log(status + ' - ' + data);
				  if ( data.trim() === '[Protocolo] = #12') {
					alert('[ERRO]: Houve Problema ao deletar, contate o Administrador.\n# - Não foi possível deletar o Veículo.');
				  }
				  if ( data.trim() === '[Protocolo] = #13') {
					alert('[INFO]: Veículo deletado com sucesso.\n# - Poderá incluir outro veículo neste usuário agora.');
					window.location.reload();
				  }
			});
			request.error(function (data, status, headers, config) {
				$scope.error = true;
				console.log(error);
			});

		} else { 
			alert("# - Cancelado com sucesso!");
		}
	}

	$scope.deletechecklist = function(obj) {

		var idchecklist = (obj.target.attributes.data.value);

		if (confirm("[INFO] - Delete CheckList.\n# - Deseja Deletar a ID = " + idchecklist + "?") == true) {

			var formData = { 
				'idchecklist' : idchecklist
			};

			var postData = 'myData='+JSON.stringify(formData);

			var request = $http({
				method: "POST",
				url: './php/checklist-delete.php',
				data: postData,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			});

			request.success(function (data, status, headers, config) {
				$scope.success = true;
				//console.log(status + ' - ' + data);
				  if ( data.trim() === '[Protocolo] = #18') {
					alert('[ERRO]: Houve Problema ao deletar, contate o Administrador.\n# - Não foi possível deletar o CheckList.');
				  }
				  if ( data.trim() === '[Protocolo] = #19') {
					alert('[INFO]: CheckList deletado com sucesso.\n# - Poderá incluir outro CheckList neste usuário agora.');
					window.location.reload();
				  }
			});
			request.error(function (data, status, headers, config) {
				$scope.error = true;
				console.log(error);
			});

		} else { 
			alert("# - Cancelado com sucesso!");
		}
	}

}]);
