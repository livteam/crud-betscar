	
<?php 

header('Content-Type:text/html;charset=UTF-8');

	include("../class/conexao.class.php");

	$myData = json_decode($_POST['myData'], true);

	$checkid = $myData['checkid'];

	$sql = "SELECT * FROM `CHECKLIST` INNER JOIN `USUARIO` ON (CHECKLIST.id_usuario = USUARIO.id_usuario) INNER JOIN `VEICULO` ON (CHECKLIST.id_veiculo = VEICULO.id_veiculo) INNER JOIN `CIDADES` ON (VEICULO.id_cidade = CIDADES.id_cidade) INNER JOIN `ITEM` ON (CHECKLIST.id_item = ITEM.id_item) INNER JOIN `MODELO` ON (VEICULO.id_modelo = MODELO.id_modelo) INNER JOIN `FABRICANTE` ON (MODELO.id_fabricante = FABRICANTE.id_fabricante) WHERE id_checklist = '$checkid'";

	$user = $conn->query($sql) or die("[Protocolo] = #8");

	while($linha = $user->fetch_assoc()) {

		$dados['id_usuario'] = $linha['id_usuario'];
		$dados['user_nome'] = $linha['user_nome'];

		$dados['id_veiculo'] = $linha['id_veiculo'];
		$dados['model_modelo'] = $linha['model_modelo'];

		$dados['id_checklist'] = $linha['id_checklist'];
		$dados['check_condicao'] = $linha['check_condicao'];
		$dados['check_funcionamento'] = $linha['check_funcionamento'];
		$dados['check_conservacao'] = $linha['check_conservacao'];
		$dados['check_comment'] = $linha['check_comment'];

		$dados['id_item'] = $linha['id_item'];
		$dados['nome_item'] = $linha['nome_item'];	
	}

	echo json_encode($dados);

	$conn->close();
?>
